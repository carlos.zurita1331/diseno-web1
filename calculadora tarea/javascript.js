

let btns = document.querySelectorAll(".num-button");
let allBtns = document.querySelectorAll(".button");
let resultBox = document.querySelector("#result-box");
let clearBtn = document.querySelector('#clear');

let total = document.querySelector("#total");

let btnSpread = [...btns];
let allBtnSpread = [...allBtns];

// Para la entrada de numeros
btnSpread.forEach((button, i) => {
  button.addEventListener("click", () => {
    // copiar los valores en la calculadora

    if (resultBox.innerHTML == "0") {
      resultBox.innerHTML = "";
    }

    let value = btns[i].innerHTML;
    resultBox.innerHTML += value;
  });
});


// funcion para evaluar cada elemento
function evaluate(fn) {
    return new Function('return ' + fn)();
}

// calcula y retorna el resultado
total.addEventListener('click', ()=> {
let allInputs = resultBox.innerHTML;

resultBox.innerHTML = evaluate(allInputs);

console.log(evaluate(allInputs));
})

// borrar todos los elementos
clearBtn.addEventListener('click', ()=> {
    resultBox.innerHTML = "0";
})