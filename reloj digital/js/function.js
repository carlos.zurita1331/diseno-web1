function FECHA(){
  var fecha = new Date ();
  var dia = fecha.getDate();
  var diaTexto = fecha.getDay();
  var meses = fecha.getMonth();
  var anio =fecha.getFullYear();

  var DIAS = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
  var MESES = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

document.getElementById("inputFecha").value = DIAS[diaTexto]+" "+dia+" de "+ MESES [meses] +" de "+anio;

setTimeout("FECHA()",1000);
}

class RelojDigital {
  constructor(elementos) {
    this.elementos = elementos
  }

  inicio()  {
    setInterval (() => {
      this.mejora();
    },1000);
  }

  mejora(){
    const partes = this.partesDelTiempo();
    const FormatoMinutos = partes.minutos.toString().padStart (2,"0");
    const FormatoSeg = partes.segundos.toString().padStart(2,"0");
    const tiempoFormado =`${partes.hora}:${FormatoMinutos}:`
    const amPm = partes.Am ? "AM" : "PM";

    this.elementos.querySelector(".tiempo-horas").textContent = tiempoFormado;
    this.elementos.querySelector(".tiempo-ampm").textContent = amPm;
    this.elementos.querySelector(".segundos").textContent = FormatoSeg;

  }

    partesDelTiempo() {
      const nuevo = new Date();
        return{
          hora: nuevo.getHours () % 12 || 12,
          minutos:nuevo.getMinutes(),
          segundos:nuevo.getSeconds(),
          Am:nuevo.getHours () < 12
        };
    }
}

const elementoReloj = document.querySelector (".hora-actual");
const ObjetoReloj = new RelojDigital (elementoReloj);

ObjetoReloj.inicio ();




