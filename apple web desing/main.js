const searchButton = document.querySelector('nav .nav-top .link-search');
const closeButton = document.querySelector('.search-container .link-close');
const navTop = document.querySelector('.nav-top');
const searchContainer = document.querySelector('.search-container');
const Overlay = document.querySelector('.overlay');

searchButton.addEventListener('click',() => {
    navTop.classList.add('hide');
    searchContainer.classList.remove('hide');
    Overlay.classList.add('show')
})

Overlay.addEventListener('click',() => {
    navTop.classList.remove('hide');
    searchContainer.classList.add('hide');
    Overlay.classList.remove('show')
})
